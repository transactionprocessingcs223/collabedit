package edu.uci.cs223.collabedit.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import edu.uci.cs223.collabedit.pps.DeleteRequest;
import edu.uci.cs223.collabedit.pps.InsertRequest;
import edu.uci.cs223.collabedit.pps.PPSItem;
import edu.uci.cs223.collabedit.pps.PartialPersistentSequence;
import edu.uci.cs223.collabedit.service.PPSService;
import edu.uci.cs223.collabedit.service.ServiceUtils;

@Controller
@RequestMapping("/pps")
public class PPSController {
    @Autowired
    private PPSService ppsService;
    
    @RequestMapping(value="/", method = RequestMethod.GET)
    public @ResponseBody PartialPersistentSequence getPPS(){
        return ppsService.getPPS();
    }
    
    @RequestMapping(value="/userid", method=RequestMethod.GET)
    public @ResponseBody Integer getUserID(){
        return ppsService.getUserID();
    }
    
    @RequestMapping(value="/userid/{userID}", method=RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void removeUserID(@PathVariable("userID") Integer userID){
        System.out.println("In PPSController.removeUserID(Integer) method");
        ppsService.removeUserID(userID);
    }
    
    @RequestMapping(value="/getupdate", method = RequestMethod.GET)
    public SseEmitter sendUpdate(@RequestParam("userID")Integer userID){
        System.out.println("In SendUpdate method");
        SseEmitter emitter = ServiceUtils.getPpsSseEmitter(userID);
        return emitter;
    }
    
    @RequestMapping(value="/insert", method = RequestMethod.POST)
    public @ResponseBody PPSItem insertText(@RequestBody InsertRequest insertRequest) throws IOException{
        return ppsService.add(insertRequest.getStartPositionStamp(), 
                insertRequest.getEndPositionStamp(), insertRequest.getItem(), insertRequest.getUserID());
        
    }
    
    @RequestMapping(value="/delete", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public void deleteText(@RequestBody DeleteRequest deleteRequest) throws IOException{
        ppsService.hide(deleteRequest.getPositionStamp(), deleteRequest.getUserID());
    }
    
    
    @RequestMapping(value="/testemit", method = RequestMethod.GET)
    public @ResponseBody String testEmit() throws IOException{
        ppsService.testEmit();
        return "ok";
    }
    
//    @RequestMapping(value = "/insert", method = RequestMethod.POST)
//    public @ResponseBody List<SearchResult> getSearchResults(
//            @RequestParam(value="query", required=true) String query,
//            @RequestParam(value="maxresults", required = false, defaultValue="10") String maxResults ) 
//                    throws NumberFormatException, IOException{
//        List<SearchResult> results = searchService.getRelevantDocuments(query, Integer.parseInt(maxResults));
//        
//        return results;
//    }
}
