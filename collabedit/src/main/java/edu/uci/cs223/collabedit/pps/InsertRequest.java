package edu.uci.cs223.collabedit.pps;

public class InsertRequest {
    private double startPositionStamp;
    private double endPositionStamp;
    private char item;
    private Integer userID;
    
    public double getStartPositionStamp() {
        return startPositionStamp;
    }
    public void setStartPositionStamp(double startPositionStamp) {
        this.startPositionStamp = startPositionStamp;
    }
    public double getEndPositionStamp() {
        return endPositionStamp;
    }
    public void setEndPositionStamp(double endPositionStamp) {
        this.endPositionStamp = endPositionStamp;
    }
    public char getItem() {
        return item;
    }
    public void setItem(char item) {
        this.item = item;
    }
    public void setUserID(Integer userID) {
        this.userID = userID;
    }
    public Integer getUserID() {
        return userID;
    }
}
