package edu.uci.cs223.collabedit.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

public class ServiceUtils {
    
//    private static SseEmitter ppsSseEmitter;
    private static Integer userID = 0;
    private static Map<Integer, SseEmitter> userIdVsEmitter  = new HashMap<Integer, SseEmitter>();
    
    public static SseEmitter getPpsSseEmitter(Integer userId){
        SseEmitter sseEmitter = userIdVsEmitter.get(userID);
        if(sseEmitter == null){
            synchronized (ServiceUtils.userIdVsEmitter) {
                if(sseEmitter == null){
                    sseEmitter = new SseEmitter(Long.MAX_VALUE);
                    userIdVsEmitter.put(userID, sseEmitter);
                }
            }
        }
        return sseEmitter;
    }
    
    public static Map<Integer, SseEmitter> getUserIdVsEmitter() {
        return userIdVsEmitter;
    }
    
    public static Integer generateUserID() {
        synchronized (ServiceUtils.userID ) {
            userID = userID + 1;
        }
        return userID;
    }
    
    public static Integer getNumUsers(){
        return userID;
    }

    public static void deleteUserID(Integer userID) {
        userIdVsEmitter.remove(userID);
        
    }
    
}
