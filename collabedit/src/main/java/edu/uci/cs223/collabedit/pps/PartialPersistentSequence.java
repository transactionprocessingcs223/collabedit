package edu.uci.cs223.collabedit.pps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class PartialPersistentSequence {
    @JsonIgnore
    private PPSItem head;
    @JsonIgnore
    private PPSItem tail;
    @JsonIgnore
    private Map<Double, PPSItem> positionStampVsPPSItem;
    @JsonIgnore
    private char defaultChar;
    
    //Maintaining this list only for the purpose of sending it to UI
    private List<PPSItem> ppsItems;
    private String text;
    
    public PartialPersistentSequence(){
        double headPositionStamp = 0;
        double tailPositionStamp = 10000;
        double sPositionStamp = generatePositionStamp(headPositionStamp, tailPositionStamp);
        double aPositionStamp = generatePositionStamp(sPositionStamp, tailPositionStamp);
        double mPositionStamp = generatePositionStamp(aPositionStamp, tailPositionStamp);
        double pPositionStamp = generatePositionStamp(mPositionStamp, tailPositionStamp);
        double lPositionStamp = generatePositionStamp(pPositionStamp, tailPositionStamp);
        double ePositionStamp = generatePositionStamp(lPositionStamp, tailPositionStamp);
        
        head = getPPSItem(headPositionStamp, defaultChar, false);
        PPSItem sItem = getPPSItem(sPositionStamp, 's', true);
        PPSItem aItem = getPPSItem(aPositionStamp, 'a', true);
        PPSItem mItem = getPPSItem(mPositionStamp, 'm', true);
        PPSItem pItem = getPPSItem(pPositionStamp, 'p', true);
        PPSItem lItem = getPPSItem(lPositionStamp, 'l', true);
        PPSItem eItem = getPPSItem(ePositionStamp, 'e', true);
        
        tail = getPPSItem(tailPositionStamp, defaultChar, false);
        
        head.setNext(sItem);
        sItem.setNext(aItem);
        aItem.setNext(mItem);
        mItem.setNext(pItem);
        pItem.setNext(lItem);
        lItem.setNext(eItem);
        eItem.setNext(tail);
        
        tail.setPrevious(eItem);
        eItem.setPrevious(lItem);
        lItem.setPrevious(pItem);
        pItem.setPrevious(mItem);
        mItem.setPrevious(aItem);
        aItem.setPrevious(sItem);
        sItem.setPrevious(head);
        
        ppsItems = new ArrayList<PPSItem>();
        ppsItems.add(head);
        ppsItems.add(sItem);
        ppsItems.add(aItem);
        ppsItems.add(mItem);
        ppsItems.add(pItem);
        ppsItems.add(lItem);
        ppsItems.add(eItem);
        ppsItems.add(tail);
        
        positionStampVsPPSItem = new HashMap<Double, PPSItem>();
        positionStampVsPPSItem.put(headPositionStamp, head);
        positionStampVsPPSItem.put(tailPositionStamp, tail);
        positionStampVsPPSItem.put(sPositionStamp, sItem);
        positionStampVsPPSItem.put(aPositionStamp, aItem);
        positionStampVsPPSItem.put(mPositionStamp, mItem);
        positionStampVsPPSItem.put(pPositionStamp, pItem);
        positionStampVsPPSItem.put(lPositionStamp, lItem);
        positionStampVsPPSItem.put(ePositionStamp, eItem);
    }
    
    private PPSItem getPPSItem(double positionStamp, char item, boolean state) {
        PPSItem ppsItem = new PPSItem();
        ppsItem.setItem(item);
        ppsItem.setState(state);
        ppsItem.setPositionStamp(positionStamp);
        return ppsItem;
    }

    public PPSUpdate add(double startPositionStamp, double endPositionStamp, char item){
        PPSItem newPPSItem = new PPSItem();
        newPPSItem.setItem(item);
        newPPSItem.setState(true);
        int index;
        PPSItem startItem = positionStampVsPPSItem.get(startPositionStamp);
        synchronized (startItem) {
            PPSItem nextItem = startItem.getNext();
            newPPSItem.setPositionStamp(
                    generatePositionStamp(startItem.getPositionStamp(), nextItem.getPositionStamp()));
            startItem.setNext(newPPSItem);
            newPPSItem.setNext(nextItem);
            newPPSItem.setPrevious(startItem);
            nextItem.setPrevious(newPPSItem);
            index = ppsItems.indexOf(startItem) + 1;
			ppsItems.add(index, newPPSItem);
        }
        positionStampVsPPSItem.put(newPPSItem.getPositionStamp(), newPPSItem);
        PPSUpdate addPPSUpdate = new PPSUpdate();
        addPPSUpdate.setPpsItem(newPPSItem);
        addPPSUpdate.setInsert(true);
        addPPSUpdate.setIndex(index);
        return addPPSUpdate;
    }
    /**
     * Splits the PPS into slots each for a single user. Adds into the corresponding slot
     * @param startPositionStamp
     * @param endPositionStamp
     * @param item
     * @param userID
     * @param totalNumUsers
     */
    public PPSUpdate addSlottedPPS(double startPositionStamp, double endPositionStamp, char item, int userID, int totalNumUsers){
    	double insertPositionStamp = getSlottedInsertPositionStamp(startPositionStamp, endPositionStamp, userID, totalNumUsers);
    	PPSItem insertPPSItem = new PPSItem();
    	insertPPSItem.setItem(item);
    	insertPPSItem.setState(true);
    	insertPPSItem.setPositionStamp(insertPositionStamp);
    	PPSItem prevPPSItem = positionStampVsPPSItem.get(startPositionStamp);
    	PPSItem nextPPSItem = prevPPSItem.getNext();
    	prevPPSItem.setNext(insertPPSItem);
    	insertPPSItem.setPrevious(prevPPSItem);
    	insertPPSItem.setNext(nextPPSItem);
    	nextPPSItem.setPrevious(prevPPSItem);
    	int index = ppsItems.indexOf(prevPPSItem) + 1;
		ppsItems.add(index, insertPPSItem);
    	positionStampVsPPSItem.put(insertPPSItem.getPositionStamp(), insertPPSItem);
    	PPSUpdate insertPPSUpdate = new PPSUpdate();
    	insertPPSUpdate.setPpsItem(insertPPSItem);
    	insertPPSUpdate.setInsert(true);
    	insertPPSUpdate.setIndex(index);
    	return insertPPSUpdate;
    }
    /**
     * Assumption is userID's are 0-indexed
     * @param startPositionStamp
     * @param endPositionStamp
     * @param userID
     * @param totalNumUsers
     * @return
     */
    private double getSlottedInsertPositionStamp(double startPositionStamp, double endPositionStamp, int userID,
			int totalNumUsers) {
    	double rightEnd = ((endPositionStamp - startPositionStamp)/totalNumUsers) * (userID + 1);
    	double leftEnd = ((endPositionStamp - startPositionStamp)/totalNumUsers) * userID;
		return startPositionStamp + ((rightEnd - leftEnd) / 2);
	}

	private double generatePositionStamp(double positionStampStart,
            double positionStampEnd) {
        return ( positionStampStart + positionStampEnd ) / 2;
    }

    public PPSUpdate hide(double positionStamp){
        PPSItem ppsItem = positionStampVsPPSItem.get(positionStamp);
        ppsItem.setState(false);
        int index = ppsItems.indexOf(ppsItem);
        ppsItems.remove(ppsItem);
        PPSUpdate hidePPSUpdate = new PPSUpdate();
        hidePPSUpdate.setPpsItem(ppsItem);
        hidePPSUpdate.setInsert(false);
        hidePPSUpdate.setIndex(index);
        return hidePPSUpdate;
    }  
    
    public List<PPSItem> getPpsItems() {
        return ppsItems;
    }
    public Map<Double, PPSItem> getPositionStampVsPPSItem() {
        return positionStampVsPPSItem;
    }
}
