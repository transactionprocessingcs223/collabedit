package edu.uci.cs223.collabedit.pps;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class PPSItem {
    private char item;
    private boolean state;
    private double positionStamp;
    @JsonIgnore
    private PPSItem next;
    @JsonIgnore
    private PPSItem previous;
    
    public char getItem() {
        return item;
    }
    public void setItem(char item) {
        this.item = item;
    }
    public boolean isState() {
        return state;
    }
    public void setState(boolean state) {
        this.state = state;
    }
    public double getPositionStamp() {
        return positionStamp;
    }
    public void setPositionStamp(double positionStamp) {
        this.positionStamp = positionStamp;
    }
    public void setNext(PPSItem next) {
        this.next = next;
    }
    public PPSItem getNext() {
        return next;
    }
    public void setPrevious(PPSItem previous) {
        this.previous = previous;
    }
    public PPSItem getPrevious() {
        return previous;
    }
    public String toString(){
    	return "char: " + item + ", state: " + state + ", positionStamp: " + positionStamp + "\n";
    }
}
