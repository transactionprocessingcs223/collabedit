package edu.uci.cs223.collabedit.pps;

public class DeleteRequest {
    private double positionStamp;
    private Integer userID;
    
    public void setPositionStamp(double positionStamp) {
        this.positionStamp = positionStamp;
    }
    
    public double getPositionStamp() {
        return positionStamp;
    }
    
    public void setUserID(Integer userID) {
        this.userID = userID;
    }
    
    public Integer getUserID() {
        return userID;
    }
    
}
