package edu.uci.cs223.collabedit.service;

import java.io.IOException;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import edu.uci.cs223.collabedit.pps.PPSItem;
import edu.uci.cs223.collabedit.pps.PPSUpdate;
import edu.uci.cs223.collabedit.pps.PartialPersistentSequence;

@Service
public class PPSService {
    
    private PartialPersistentSequence pps;
    
    public PartialPersistentSequence getPPS(){
        if(pps == null){
            synchronized (this) {
                if(pps == null){
                    pps = new PartialPersistentSequence();
                }
            }
        }
        return pps;
    }
    
    public PPSItem add(double startPositionStamp, double endPositionStamp, 
            char item, Integer userID) throws IOException{
        
//        PPSUpdate ppsUpdate = pps.add(startPositionStamp, endPositionStamp, item);
        PPSUpdate ppsUpdate = pps.addSlottedPPS(startPositionStamp, endPositionStamp, item, userID, ServiceUtils.getNumUsers());
        Map<Integer, SseEmitter> userIdVsSseEmitter = ServiceUtils.getUserIdVsEmitter();
        for(Integer current : userIdVsSseEmitter.keySet()){
            if(!current.equals(userID)){
                try {
                    userIdVsSseEmitter.get(current).send(ppsUpdate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ppsUpdate.getPpsItem();
        
    }
    
    public void hide(double positionStamp, Integer userID) throws IOException{
        PPSUpdate ppsUpdate = pps.hide(positionStamp);
        Map<Integer, SseEmitter> userIdVsSseEmitter = ServiceUtils.getUserIdVsEmitter();
        for(Integer current : userIdVsSseEmitter.keySet()){
            if(!current.equals(userID)){
                try {
                    userIdVsSseEmitter.get(current).send(ppsUpdate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void testEmit() throws IOException {
        
        Map<Integer, SseEmitter> userIdVsSseEmitter = ServiceUtils.getUserIdVsEmitter();
        for(Integer current : userIdVsSseEmitter.keySet()){
            userIdVsSseEmitter.get(current).send(pps);
        }
        
    }

    public Integer getUserID() {
        return ServiceUtils.generateUserID();
    }

    public void removeUserID(Integer userID) {
        ServiceUtils.deleteUserID(userID);
        
    }
}
