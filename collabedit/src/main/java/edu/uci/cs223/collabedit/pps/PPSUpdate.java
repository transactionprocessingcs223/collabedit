package edu.uci.cs223.collabedit.pps;

public class PPSUpdate {
    private PPSItem ppsItem;
    private boolean isInsert;
    private int index;
    public PPSItem getPpsItem() {
        return ppsItem;
    }
    public void setPpsItem(PPSItem ppsItem) {
        this.ppsItem = ppsItem;
    }
    public boolean isInsert() {
        return isInsert;
    }
    public void setInsert(boolean isInsert) {
        this.isInsert = isInsert;
    }
    public int getIndex() {
        return index;
    }
    public void setIndex(int index) {
        this.index = index;
    }
    
    
}
