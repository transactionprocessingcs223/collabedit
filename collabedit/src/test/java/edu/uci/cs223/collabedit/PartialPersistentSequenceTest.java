package edu.uci.cs223.collabedit;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import edu.uci.cs223.collabedit.pps.PPSItem;
import edu.uci.cs223.collabedit.pps.PartialPersistentSequence;

public class PartialPersistentSequenceTest {
	private PartialPersistentSequence pps;
	
	@Before
	public void setUp(){
		pps = new PartialPersistentSequence();
	}
	
	@Test
	public void smokeTest(){
		List<PPSItem> ppsItems = pps.getPpsItems();
		System.out.println(ppsItems);
	}
}
