angular.module('myApp', ['angularTrix']).controller('myController', function($scope, $http, $window){

	var startBlock = "<div><!--block-->";
	var endBlock = "</div>";
	$scope.ignoreTrixChange = false;
	$scope.ignoreTrixSelectionChange = false;
	
	$scope.getUserID = function(){
		console.log("Getting userID from server");
		return $http.get('/collabedit/rest/pps/userid').then(function(response){
			console.log("Get User ID successful");
			console.log("User ID: "+response.data);
			$scope.userID = response.data;
		})
	};
//	$scope.getUserID();
	$scope.textContent = "";
	$scope.getPPS = function(){
		var config = {};
		console.log("Getting PPS from server");
		return $http.get('/collabedit/rest/pps/', config).then(function(response){
			console.log("Get PPS successful");
			$scope.pps = response.data.ppsItems;
			for(i = 0; i < $scope.pps.length; i++){
				var ppsItem = $scope.pps[i];
				if(ppsItem.state){
					$scope.textContent += $scope.pps[i].item;
				}
			}
		});
	}
	
	$scope.pps={};
	$scope.currentCursorPosition = 0;
	$scope.textLength=0;
	
//	$scope.trixFocus = function(e, editor){
//		console.log("In TrixFocus:");
//		console.log(editor.getSelectedRange());
//		$scope.previousCursorPosition = $scope.currentCursorPosition;
//		$scope.currentCursorPosition = editor.getSelectedRange()[0];
//		console.log("$scope.currentCursorPosition:"+$scope.currentCursorPosition);
//	}
	
	$scope.trixSelectionChange = function(e, editor){
		if($scope.ignoreTrixSelectionChange){
			console.log("Ignoring TrixSelectionChange");
			$scope.ignoreTrixSelectionChange = false;
			editor.setSelectedRange($scope.currentCursorPosition);
			return;
		}
		console.log("In TrixSelctionChange");
		$scope.previousCursorPosition = $scope.currentCursorPosition;
		$scope.currentCursorPosition = editor.getSelectedRange()[0];
		console.log("$scope.currentCursorPosition:"+$scope.currentCursorPosition);
	}
	
	$scope.trixInitialize = function(e, editor){
		console.log("In Trix Initialize");
		$scope.ignoreTrixChange = true;
		var getPPSPromise = $scope.getPPS();
		getPPSPromise.then(function(){
//			editor.setSelectedRange([0, $scope.textContent.length])
		    editor.insertString($scope.textContent);
			editor.setSelectedRange($scope.textContent.length);
			$scope.currentCursorPosition = editor.getSelectedRange()[0];
			$scope.textLength = editor.getDocument().toString().length;
			console.log(editor.getSelectedRange());
//			$scope.textContent = $scope.trim($scope.textContent);
			console.log("$scope.textContent:"+$scope.textContent);
			console.log("$scope.currentCursorPosition:"+$scope.currentCursorPosition);
		});
		
	};
	
	$scope.trim = function(textContent){
		if(textContent.startsWith(startBlock)){
			textContent = textContent.substr(startBlock.length, textContent.length - endBlock.length - startBlock.length);
		}
		return textContent;
	}
	
	$scope.trixChange = function(e, editor){
		if($scope.ignoreTrixChange){
			console.log("Ignoring trix change");
			$scope.ignoreTrixChange = false;
			$scope.ignoreTrixSelectionChange = true;
			console.log("$scope.currentCursorPosition:"+$scope.currentCursorPosition);
			console.log("editor.getSelectedRange()[0]:"+editor.getSelectedRange()[0]);
			editor.setSelectedRange($scope.currentCursorPosition);
			return;
		}
//		var cursorPosition = $scope.getCaretPosition(angular.element('.textEditor'));
//		console.log(cursorPosition);
//		console.log("Reached Insert Text function");
//		console.log(e);
//		console.log(editor);
//		console.log(editor.getDocument());
//		console.log("editor.getDocument.toString(): "+editor.getDocument().toString());
//		console.log("editor.getSelectedRange(): "+editor.getSelectedRange());
		console.log("In trix change");
		editor.setSelectedRange($scope.currentCursorPosition);
		var currentLength = editor.getDocument().toString().length;
		if($scope.textLength < currentLength){
			$scope.textLength = currentLength;
			var insertedAtIndex = editor.getSelectedRange()[0] - 1;
			var characterInserted = editor.getDocument().toString().charAt(insertedAtIndex);
			console.log("Inserting " + characterInserted + " at the position:"+ insertedAtIndex);
			var ppsIndex = getPPSIndex(insertedAtIndex);
//			var counter = 0;
//			while(counter != insertedAtIndex){
//				if($scope.pps[ppsIndex].state){
//					counter ++;
//				}
//				ppsIndex++;
//			}

			console.log("Corresponding PPS Index is :"+ ppsIndex);
			var insertRequest = {"startPositionStamp": $scope.pps[ppsIndex - 1].positionStamp, 
					"item":characterInserted, "userID": $scope.userID};
			$http.post('/collabedit/rest/pps/insert', insertRequest).then(function(response){
				$scope.pps.splice(ppsIndex, 0, response.data);
				console.log("Updated the local PPS");
				console.log($scope.pps);
			});
			
//			$scope.pps.splice(ppsIndex--, 0, );
			
		}else if($scope.textLength > currentLength) {
			$scope.textLength = currentLength;
			var deletedAtIndex = editor.getSelectedRange()[0];
//			var characterChanged = editor.getDocument().toString().charAt(editor.getSelectedRange()[0]);
			console.log("Deleting at the position: "+ deletedAtIndex);
			var ppsIndex = getPPSIndex(deletedAtIndex);
//			var counter = 0;
//			while(counter != deletedAtIndex){
//				if($scope.pps[ppsIndex].state){
//					counter++;
//				}
//				ppsIndex++;
//			}
			console.log("Corresponding PPS Index is :"+ ppsIndex);
			var deleteRequest = {"positionStamp":$scope.pps[ppsIndex].positionStamp, "userID":$scope.userID};
			$http.post('/collabedit/rest/pps/delete', deleteRequest).then(function(response){
				$scope.pps.splice(ppsIndex, 1);
				console.log("Updated the local PPS");
				console.log($scope.pps);
			});
		}
//		$scope.currentCursorPosition = editor.getSelectedRange()[0];
//		console.log(editor.getDocument());
//		console.log(editor.getDocument().toString());
		
	}
	
	var getPPSIndex = function(textContentIndex){
		var ppsIndex = 0;
		var counter = 0;
		if(textContentIndex == 0){
			while(!$scope.pps[ppsIndex].state){
				ppsIndex ++;
			}
			return ppsIndex++;
		}
		while(counter != textContentIndex){
			if($scope.pps[ppsIndex].state){
				counter++;
			}
			ppsIndex++;
		}
		return ppsIndex;
	}
	
	var getTextContentIndex = function(ppsIndex){
		var textContentIndex = 0;
		var counter = 0;
		while(counter != ppsIndex){
			if($scope.pps[counter].state){
				textContentIndex++;
			}
			counter++;
		}
		return textContentIndex;
	}
	
	$scope.onExit = function() {
	      console.log("Removing the userId");
	      $http.delete('/collabedit/rest/pps/userid/'+$scope.userID).then(function(response){
	    	  console.log("Delete UserID successful");
	      });
    };
    
    var decodeText = function(input){
    	var txt = document.createElement("textarea");
        txt.innerHTML = input;
        return txt.value;
    }
    
    var getUserIDPromise = $scope.getUserID();
    getUserIDPromise.then(function(){
    	var source = new EventSource('/collabedit/rest/pps/getupdate?userID='+$scope.userID);
    	source.onmessage = function(event){
    		console.log("PPS Received from the server");
    		console.log(event.data);
    		var ppsUpdate = angular.fromJson(event.data);
    		$scope.ignoreTrixChange = true;
//    		$scope.ignoreTrixSelectionChange = true;
    		$scope.textContent = $scope.trim($scope.textContent);
    		$scope.textContent = decodeText($scope.textContent);
    		
    		if(ppsUpdate.insert){
    			$scope.pps.splice(ppsUpdate.index, 0, ppsUpdate.ppsItem);
    			var textContentIndex = getTextContentIndex(ppsUpdate.index);
    			
    			$scope.textContent = $scope.textContent.splice(textContentIndex, 0, ppsUpdate.ppsItem.item);
    			
    		}else{
    			$scope.pps.splice(ppsUpdate.index, 1);
    			var textContentIndex = getTextContentIndex(ppsUpdate.index);
    			$scope.textContent = $scope.textContent.splice(textContentIndex, 1);
    		}
    		console.log("Updated TextContent:"+$scope.textContent);
    		$scope.$apply();
//    		for(i = 0; i < $scope.pps.length; i++){
//				var ppsItem = $scope.pps[i];
//				if(ppsItem.state){
//					$scope.textContent += $scope.pps[i].item;
//				}
//			}
//    		$scope.trixInitialize();
    	}
    })

   $window.onbeforeunload =  $scope.onExit;
    String.prototype.splice = function(idx, rem, str) {
    	if(str){
            return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
    	}else{
            return this.slice(0, idx) + this.slice(idx + Math.abs(rem));
    	}
    };
});